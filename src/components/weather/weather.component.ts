import weatherController from './weather.controller';
import './weather.scss';

declare var require: (filename: string) => any;

export class weatherComponent {
  public bindings: any = {};
  public controller: any = weatherController;
  public controllerAs: string = '$weather';
  public template: string = require('./weather.html');

  constructor() {}
}

export default weatherComponent;
