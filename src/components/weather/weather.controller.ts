import {chunk} from 'lodash';
import * as moment from 'moment';

import config from '../../config';

export class weatherController {
  public name = 'hamid';
  public forecast:any = null;
  public q:string = '';
  public unit:string = 'c';
  public dayNumber:number = 0;
  public hourNumber:number = 0;
  public days:any[] = [];
  public moment = moment;

  constructor(
    private weatherService,
    private $interval
  ) {
    this.search('Bangalore, IN');
  }

  search = q => {
    return this.weatherService.getCurrent(q).then(res => {
      this.dayNumber = 0;
      this.hourNumber = 0;
      this.forecast = res;
      this.days = chunk(this.forecast.list, 8);
    }, err => {
      console.error('error', err);
      if(err.data) alert(err.data.message);
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    this.search(this.q).then(res => this.q='', err => this.q='');
  }

  toggleUnit = () => {
    this.unit = this.unit == 'c' ? 'f':'c';
  }

  getTemp = (temp:any=0, deg = 'c') => {
    switch (deg) {
      case "c":
        return Math.round(temp - 273.15);
      case "f":
        return Math.round((temp - 273.15)* 1.8);
    }
  }

  getDay = (dayNumber=null) => {
    if(dayNumber === null) dayNumber = this.dayNumber;
    return this.days[dayNumber];
  }

  getDayHour = (dayNumber=0, hourNumber=0) => {
    const day = this.getDay(dayNumber);
    // if(hourNumber==1)
      // console.log('dayNumber', dayNumber, 'hourNumber', hourNumber, 'day', day, 'hour', day? day[hourNumber].dt:'');
    if(day) return day[hourNumber];
    return null;
  }

  getDayOrNight = ({id, icon}) => {
    if(icon && icon.charAt(icon.length - 1) == 'n') {
      return 'night';
    }
    return 'day';
  }

  getIcon = (weather) => {
    const {id=0} = weather || {};
    const prefix = 'wi wi-';
    let {icon=''} = config.weatherIcons[id] || {};

    // If we are not in the ranges mentioned above, add a day/night prefix.
    if (!(id > 699 && id < 800) && !(id > 899 && id < 1000)) {
      icon = 'day-' + icon;
    }

    // Finally tack on the prefix.
    return prefix + icon;
  }

}
weatherController.$inject = ['weatherService', '$interval'];

export default weatherController;