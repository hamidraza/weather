import config from '../../config';

export class WeatherService {

  constructor(private $http) {
    // this.getCurrent();
  }

  getCurrent($location = '') {
    return this.$http({
      method: 'GET',
      url: 'https://0cmjcy5nxd.execute-api.ap-southeast-1.amazonaws.com/p/data/2.5/forecast',
      params: {
        q: $location || 'Bangalore,in',
        appid: config.weatherApiKey
      }
    }).then(res => res.data);
  }

}
WeatherService.$inject = ['$http'];

export default WeatherService;
