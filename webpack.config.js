module.exports = {
  entry: './src/app.ts',
  output: {
    path: './dist/build',
    publicPath: '/build/',
    filename: 'bundle.js'
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['', '.webpack.js', '.web.js', '.ts', '.tsx', '.js']
  },
  module: {
    loaders: [
      {test: /\.ts(x?)$/, loader: 'ts-loader'},
      {test: /\.scss$/, loader: 'style!css!sass'},
      {test: /\.html$/, loader: 'raw', exclude: /node_modules/}
    ]
  },
  devServer: {
    inline: true,
    contentBase: './dist',
    port: 3000,
    historyApiFallback: 'index.html',
    stats: {
      chunks: false
    }
  }
}
