import 'angular';
import './app.scss';

import weatherApp from './components/weather';

angular.bootstrap(document, [weatherApp.name], { strictDi: true });
