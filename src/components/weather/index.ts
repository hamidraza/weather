import weatherController from './weather.controller';
import weatherComponent from './weather.component';
import WeatherService from './weather.service';

export default angular.module('weatherApp', [])
  .service('weatherService', WeatherService)
  .controller('weatherComponentCtrl', weatherController)
  .component('weatherComponent', new weatherComponent());

