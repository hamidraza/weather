const appName = 'app';

const module = function(moduleOrName) {
  return typeof moduleOrName === "string" ?
    angular.module(moduleOrName) :
    moduleOrName;
}

export function Component(options: {
  selector: string,
  controllerAs?: string,
  template?: string,
  templateUrl?: string,
  bindings?: any,
  require?: any,
  directives?: any[],
  pipes?: any[],
  providers?: any[]
}, moduleOrName: string | ng.IModule = `${appName}.component`) {
  return (Class: any) => {
    const selector = options.selector;
    delete options.selector;
    delete options.directives;
    delete options.pipes;
    delete options.providers;
    Class.selector = selector;
    module(moduleOrName).component(selector, angular.extend(options, { controller: Class }));
  }
};
